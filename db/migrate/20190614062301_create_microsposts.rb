class CreateMicrosposts < ActiveRecord::Migration[5.1]
  def change
    create_table :microsposts do |t|
      t.text :conent
      t.integer :user_id

      t.timestamps
    end
  end
end
